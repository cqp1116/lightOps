<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<link rel="stylesheet" href="${webRoot}/plug-in/mutitables/datagrid.menu.css" type="text/css"></link>
<style>.datagrid-toolbar{padding:0 !important;border:0}</style>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:0px;border:0px">
  <t:datagrid name="sqlTaskList" filterBtn="true" checkbox="true" fitColumns="false" title="" sortName="createDate" sortOrder="desc" actionUrl="sqlTaskController.do?datagrid" idField="id" fit="true" queryMode="group" onDblClick="datagridDbclick" onLoadSuccess="optsMenuToggle" pagination="false">
  
   <t:dgCol title="操作" field="opt" width="47" optsMenu="true"></t:dgCol>
   <t:dgFunOpt inGroup="false" funname="curd.detail(id)" urlfont="fa-eye" title="查看详情"/>
   <%--
   <t:dgFunOpt funname="curd.addRow" urlfont="fa-plus" title="新增一行" />
   <t:dgFunOpt funname="curd.deleteOne(id)" urlfont="fa-minus" title="删除该行" />
   <t:dgFunOpt inGroup="true" exp="bpmStatus#eq#1"  funname="startProcess(id)" urlfont="fa-level-up" title="提交流程"/> --%>
   
   <t:dgCol title="主键"  field="id"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="数据库"  field="dbId" extendParams="editor:'text'" width="120" dictionary="database_info,id,db_desc"></t:dgCol>
   <t:dgCol title="SQL_ID"  field="sqlId"  hidden="true"   extendParams="editor:'text'" width="120" ></t:dgCol>
   <t:dgCol title="执行状态"  field="executeFlag"  dictionary="exec_flag" filterType="combobox" extendParams="editor:'combobox'" width="120"></t:dgCol>
   <t:dgCol title="执行结果"  field="executeResult"  filterType="combobox" extendParams="editor:'combobox'" width="120"></t:dgCol>
   <t:dgCol title="执行时间"  field="executeDate"  filterType="datebox" extendParams="editor:{type:'datebox',options:{onShowPanel:initDateboxformat}}" width="120" formatter="yyyy-MM-dd" ></t:dgCol>
   <t:dgCol title="创建人名称"  field="createName"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="创建人登录名称"  field="createBy"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="创建日期"  field="createDate"  formatter="yyyy-MM-dd"  hidden="true"  filterType="datebox" extendParams="editor:{type:'datebox',options:{onShowPanel:initDateboxformat}}" width="120"></t:dgCol>
   <t:dgCol title="更新人名称"  field="updateName"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="更新人登录名称"  field="updateBy"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="更新日期"  field="updateDate"  formatter="yyyy-MM-dd"  hidden="true"  filterType="datebox" extendParams="editor:{type:'datebox',options:{onShowPanel:initDateboxformat}}" width="120"></t:dgCol>
   <t:dgCol title="所属部门"  field="sysOrgCode"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="所属公司"  field="sysCompanyCode"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="流程状态"  field="bpmStatus"  hidden="true"  dictionary="bpm_status" filterType="combobox" extendParams="editor:'combobox'" width="120"></t:dgCol>
   
   <t:dgToolBar title="执行" icon="icon-blank" url="sqlTaskController.do?executeSql" funname="executeSQL" width="770"></t:dgToolBar>
   
  </t:datagrid>
  </div>
  	<input type="hidden" id = "sqlTaskListMainId"/>
</div>
  <script type="text/javascript" src="${webRoot}/plug-in/mutitables/mutitables.urd.js"></script>
  <script type="text/javascript" src="${webRoot}/plug-in/mutitables/mutitables.curdInIframe.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
	  curd = $.curdInIframe({
		  name:"sqlTask",
		  describe:"执行任务",
		  urls:{
			  excelImport:'sqlListController.do?commonUpload'
		  }
	  });
	  gridname = curd.getGridname();
 	});
 	
  //双击行编辑
  function datagridDbclick(index,row,dgname){
	  $("#sqlTaskList").datagrid('beginEdit', index);
	  afterRowEdit(index);
  }
  
   /**
	* dataGrid 操作菜单 切换显示onloadSuccess 调用*/
   function optsMenuToggle(data){
  	  optsMenuToggleBydg($("#sqlTaskList").datagrid('getPanel'));
   }
   
  /**
   * 行编辑开启后需要处理相关列的选择事件
   * @editIndex 行编辑对应的行索引
   */
  function afterRowEdit(editIndex){
	  //TODO 
  }
 	
 //导出模板
 function exportExcelTemplate(){
	JeecgExcelExport("sqlTaskController.do?exportXlsByT","sqlTaskList");
 }
 //导出
 function ExportXls() {
	JeecgExcelExport("sqlTaskController.do?exportXls","sqlTaskList");
 }
 

		
	function executeSQL(title, url, id) {
		var rows = $('#' + id).datagrid('getSelections');
		if (rows.length == 0) {
			alert('请选择需要操作的数据库');
			return;
		}
		var ids = [];
		for(var i=0; i<rows.length; i++){
			ids.push(rows[i].id);
		}
		
		var sqlId = $("#sqlTaskListMainId").val();
		$.ajax({
	     	type: "POST",
	        url: url+"&dbIds="+ ids+"&sqlId="+ sqlId,
	        dataType:"text",
	        contentType: 'charset=utf-8',
	        success: function(option){
	        	$('#sqlTaskList').datagrid('reload');
	       	}
	       });    
		
	}
</script>

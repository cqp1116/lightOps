package com.gsafety.devops.service;
import com.gsafety.devops.entity.PortScanEntity;
import org.jeecgframework.core.common.service.CommonService;

import java.io.Serializable;

public interface PortScanServiceI extends CommonService{
	
 	public void delete(PortScanEntity entity) throws Exception;
 	
 	public Serializable save(PortScanEntity entity) throws Exception;
 	
 	public void saveOrUpdate(PortScanEntity entity) throws Exception;
 	
}
